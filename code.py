# code.py
# https://learn.adafruit.com/modal-midi-keyboard/how-it-works
# adafruit midi
# https://docs.circuitpython.org/projects/midi/en/latest/api.html
# controller change https://learn.adafruit.com/grand-central-usb-midi-controller-in-circuitpython/code-usb-midi-in-circuitpython

# port com visible : 115200 bds
import board
import digitalio # https://docs.circuitpython.org/en/latest/shared-bindings/digitalio/index.html
import pwmio
import time
import usb_midi
import adafruit_midi
import rotaryio # rotatif https://learn.adafruit.com/rotary-encoder/circuitpython

# matrice de boutton  https://learn.adafruit.com/matrix-keypad/python-circuitpython installer la lib
import adafruit_matrixkeypad 
rows = [digitalio.DigitalInOut(x) for x in (board.GP2, board.GP3, board.GP5, board.GP7)]
cols = [digitalio.DigitalInOut(x) for x in (board.GP0, board.GP1, board.GP4 , board.GP6)]
keys = ((1, 2, 3, 4),  # pour identifier who is pressed
        (21, 22, 23, 24),
        (31, 32, 33, 34),
        (41,42,43,44))
keypad = adafruit_matrixkeypad.Matrix_Keypad(rows, cols, keys)

print("Simple piHpsdr Midi controller channel 3")

#import midi
from adafruit_midi.note_on import NoteOn
from adafruit_midi.note_off import NoteOff
from adafruit_midi.control_change import ControlChange # pour reconnaissance des commandes roue VFO

# changement de channel en 3 au lieu de 0 comme exemple hpsdr midi
midi = adafruit_midi.MIDI(midi_out=usb_midi.ports[1], out_channel=3)

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

#codeur rotatif
encoder1 = rotaryio.IncrementalEncoder(board.GP11, board.GP10) # VFO
last_position1 = 0
encoder2 = rotaryio.IncrementalEncoder(board.GP18, board.GP19)
last_position2 = 0
encoder3 = rotaryio.IncrementalEncoder(board.GP17, board.GP16)
last_position3 = 0
encoder4 = rotaryio.IncrementalEncoder(board.GP13, board.GP12)
last_position4 = 0
encoder5 = rotaryio.IncrementalEncoder(board.GP14, board.GP15)
last_position5 = 0

reverse1=-1 # 1 ou -1 pour selectionner le sens de rotation
reverse2=-1
reverse3=-1
reverse4=-1
reverse5=-1

while True:
    keys = keypad.pressed_keys # renvoie une liste pas une simple valeure
    if keys:
        led.value = True
        val=keys[0]
        midi.send(NoteOn(val, 60))
        print("Pressed: ", keys)
        midi.send(NoteOff(val, 0))
        time.sleep(0.2) #0.1 un peu trop rapide
        led.value = False

# lecture avec controller change
    position1 = encoder1.position  # VFO
    if position1 != last_position1:
        print("pos:",position1, " last:",last_position1)
        # ne pas comparer avec du none.
        if (position1 < last_position1):
            midi.send(ControlChange(11,64+reverse1))
            print("down1..")
        if (position1 > last_position1):
            midi.send(ControlChange(11,64-reverse1))
            print("up1.")
        last_position1 = position1
        
    time.sleep(0.02) # pour diminuer le nombre d'envoie de datas

    position2 = encoder2.position
    if position2 != last_position2:
        print(int(position2))
        # ne pas comparer avec du none.
        if (position2 < last_position2):
            midi.send(ControlChange(12,64+reverse2))
            print("down2..")
        if (position2 > last_position2):
            midi.send(ControlChange(12,64-reverse2))
            print("up2..")
        last_position2 = position2

    position3 = encoder3.position
    if position3 != last_position3:
        print(int(position3))
        # ne pas comparer avec du none.
        if (position3 < last_position3):
            midi.send(ControlChange(13,64+reverse3))
            print("down3..")
        if (position3 > last_position3):
            midi.send(ControlChange(13,64-reverse3))
            print("up3..")
        last_position3 = position3
			
    position4 = encoder4.position
    if position4 != last_position4:
        print(int(position4))
        # ne pas comparer avec du none.
        if (position4 < last_position4):
            midi.send(ControlChange(14,64+reverse4))
            print("down4..")
        if (position4 > last_position4):
            midi.send(ControlChange(14,64-reverse4))
            print("up4..")
        last_position4 = position4
		
    position5 = encoder5.position
    if position5 != last_position5:
        print(int(position5))
        # ne pas comparer avec du none.
        if (position5 < last_position5):
            midi.send(ControlChange(15,64+reverse5))
            print("down5..")
        if (position5 > last_position5):
            midi.send(ControlChange(15,64-reverse5))
            print("up5..")
        last_position5 = position5

	


