# Simple Midi controller for Pihpsdr and Radioberry with Pico RP2040
Send midi command and midi key on off through USB. Can be used with pihpsdr and Radioberry

Radioberry : https://github.com/pa3gsb/Radioberry-2.x/wiki

Cpython from Adafruit with Midi lib:
https://learn.adafruit.com/modal-midi-keyboard/code-the-modal-midi-controller

## hardware

Midi controller original schematic idea from VU2DLE : https://github.com/VU2DLE/Radioberry_Console
https://github.com/VU2DLE/Radioberry_Console/blob/main/Hardware/Schematic/RB_Console_Sch_V10.pdf

simple schematic, pull up are not mandatory. No need PCB : see simple proto picture

## Installation

Install Cpython on Pico RP2040. Follow example :

https://learn.adafruit.com/modal-midi-keyboard/installing-circuitpython

Add lib matrix_keypad in Pico  /lib

https://github.com/adafruit/Adafruit_CircuitPython_MatrixKeypad

Simply replace code.py in Pico /

## Usage

you can monitor activity on serial port at 115200 bds

You can monitor midi input through USB with :

aseqdump -l  
aseqdump -p Pico

After starting pihpsdr go to midi configuration tool : load file Pico CircuitPython usb_midi.por.midi or define it with learning tool.

No compilation : you can edit the code.py to adjust value. Save to register and restart.


